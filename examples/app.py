# -*- coding: utf-8 -*-
from flask import Flask, render_template, request

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, BooleanField, PasswordField
from wtforms.validators import DataRequired, Length

from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy

import requests
import json

app = Flask(__name__)
app.secret_key = 'dev'

bootstrap = Bootstrap(app)
db = SQLAlchemy(app)

url = "http://127.0.0.1:5001/company/details"

class HelloForm(FlaskForm):
    username = StringField('Feeling lucky?', validators=[DataRequired(), Length(1, 200)])
    #password = PasswordField('Password', validators=[DataRequired(), Length(8, 150)])
    #remember = BooleanField('Remember me')
    submit = SubmitField()


class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)


@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')



@app.route('/nav', methods=['GET', 'POST'])
def test_nav():
    return render_template('nav.html')


@app.route('/pagination', methods=['GET', 'POST'])
def test_pagination():
    db.drop_all()
    db.create_all()
    for i in range(100):
        m = Message()
        db.session.add(m)
    db.session.commit()
    page = request.args.get('page', 1, type=int)
    pagination = Message.query.paginate(page, per_page=10)
    messages = pagination.items
    return render_template('pagination.html', pagination=pagination, messages=messages)


@app.route('/utils', methods=['GET', 'POST'])
def test_utils():
    return render_template('utils.html')

#==================================================
#==================================================
#==================================================
# ONLY CARE ABOUT THINGS BELOW


@app.route('/form', methods=['GET', 'POST'])
def test_form():
    if request.method == 'GET':
        form = HelloForm()
        return render_template('form.html', form=form)
    else:
        # This is where the meat happens
        company = request.form['username']

        data = {'company_name': company}

        #print(data)
        # 1. call core service and get a response
        #data = {'company_name': 'We did it!'}
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

        text_response = requests.post(url, data=json.dumps(data), headers=headers).text

        print(text_response)

        json_response = json.loads(text_response)

       

        competitors = json_response['competitors']
        company_name = json_response['name']
        scores = json_response['scores']
        news = json_response['recent_press_coverage']
        
        # # 2. render new page with data from the core service response
        return render_template('final.html', company_name=company_name, scores=scores, competitors=competitors, news=news)
        #return render_template('index.html')

# @app.route('/form/<company_name>', methods=['POST'])
# def submit_company(company_name, scores, competitors, news):
#     return render_template('final.html', company_name=company_name, scores=scores, competitors=competitors, news=news)
